package conicle

import (
	"crypto/tls"
	"log"
	"net/http"
	"time"

	"github.com/tiaguinho/gosoap"
)

// CheckAgentRequest Data
type AddRequest struct {
	IntA string
	IntB string
}

// SoapBuildRequest build data request
func (r AddRequest) SoapBuildRequest() *gosoap.Request {
	return gosoap.NewRequest("Add", gosoap.Params{
		"intA": r.IntA,
		"intB": r.IntB,
	})
}

// AddResponse Data
type AddResponse struct {
	Result string `xml:"AddResult"`
}

// SOAPSample shows how a typical application can verify a login attempt
func SOAPAuthenticate(username string, password string) (map[string]interface{}, error) {

	var res *gosoap.Response

	soap, err := gosoap.SoapClientWithConfig("http://www.dneonline.com/calculator.asmx?wsdl",
		&http.Client{
			Timeout: 4 * time.Minute,
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					InsecureSkipVerify: true,
				},
				MaxIdleConns:    10,
				IdleConnTimeout: 30 * time.Second,
			}},
		&gosoap.Config{Dump: true},
	)
	if err != nil {
		log.Printf("Err: %+v", err)
	}
	var addRes AddResponse
	var user map[string]interface{}
	res, err = soap.CallByStruct(AddRequest{
		IntA: "1",
		IntB: "2",
	})
	if err != nil {
		log.Printf("Err1: %+v", err)
	} else {
		res.Unmarshal(&addRes)
		log.Printf("User: %+v", addRes)
	}
	return user, err
}
