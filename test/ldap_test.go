package conicle

import (
	"log"

	"github.com/jtblin/go-ldap-client"
)

// LDAPAuthenticate shows how a typical application can verify a login attempt
func LDAPAuthenticate(username string, password string) (map[string]interface{}, error) {
	client := &ldap.LDAPClient{
		Base:         "OU=Bussiness Units,DC=muangthai,DC=co,DC=th",
		Host:         "muangthai.co.th",
		ServerName:   "muangthai.co.th",
		Port:         636,
		UseSSL:       true,
		SkipTLS:      true,
		BindDN:       "sys-conicle-uat",
		BindPassword: "KFfb$bdfDdv34#1kdf8",
		UserFilter:   "(sAMAccountName=%s)",
		Attributes:   []string{"employeeID", "userPrincipalName", "dn"},
	}
	defer client.Close()

	var user map[string]interface{}
	log.Println("Start")
	ok, res, err := client.Authenticate(username, password)
	log.Println("End")
	if ok {
		log.Printf("Res: %+v", res)
		if err != nil {
			log.Printf("Err: %+v", err)
		}
	}
	return user, err
}
