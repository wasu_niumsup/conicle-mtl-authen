package models

import (
	"techberry-go/common/facade"
)

// InputRoot is root data
/*
{
	"data": {
		"name" : "aaa"
	}
}
*/
type InputRoot struct {
	facade.DataContext
	//Data struct {
	Username string `structs:"username" json:"username" bson:"username"`
	Password string `structs:"password" json:"password" bson:"password"`
	//} `structs:"data" json:"data" bson:"data"`
}
