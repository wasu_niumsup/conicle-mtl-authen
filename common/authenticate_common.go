package common

import "regexp"

// GetAccountType to get type from username
func (c *ServiceCommon) GetAccountType(username string) (key string, err error) {
	var validID = regexp.MustCompile(`^[0-9]+$`)
	var size = len(username)
	var numberOnly = validID.MatchString(username)
	// c.Context.Info("%s %v %v", username, size, numberOnly)
	if size == 6 && numberOnly {
		key = "agent"
	} else if size > 0 {
		key = "staff"
	}
	return

}
