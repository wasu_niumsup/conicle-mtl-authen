package common

import (
	"context"
	"techberry-go/common/facade"
)

// ServiceCommon is the utils controller.
type ServiceCommon struct {
	TraceContext context.Context
	Context      facade.AppContext
	Properties   facade.Properties
	DataBus      facade.DataBus
	Connector    facade.Connector
	Handler      facade.Handler
	Version      string
}
