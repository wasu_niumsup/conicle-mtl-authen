package controllers

import (
	"techberry-go/common/facade"

	"sandbox/conicle-mtl-authen/accessors"
	"sandbox/conicle-mtl-authen/common"
)

// ServiceController is the main controller
type ServiceController struct {
	Context         facade.AppContext
	Properties      facade.Properties
	DataBus         facade.DataBus
	Connector       facade.Connector
	Handler         facade.Handler
	Version         string
	ServiceAccessor accessors.ServiceAccessor
	ServiceCommon   common.ServiceCommon
}
