package controllers

import (
	"errors"
	"sandbox/conicle-mtl-authen/models"
	"strings"
)

// Authenticate verify a login
func (c *ServiceController) Authenticate() {
	// Binding request data
	inputRoot := &models.InputRoot{}
	c._bindInputRoot(inputRoot)

	username := inputRoot.Username
	password := inputRoot.Password
	var response map[string]interface{}
	var err error
	if len(username) == 0 || len(password) == 0 {
		err = errors.New("Length Required or Invalid Request")
	} else {
		var accType string
		accType, err = c.ServiceCommon.GetAccountType(username)
		if accType == "staff" { // check Staff
			response, err = c.ServiceAccessor.LDAPAuthenticate(username, password)
		} else if accType == "agent" { // check Agent
			response, err = c.ServiceAccessor.SOAPAuthenticate(username, password)
		} else {
			err = errors.New("Length Required or Invalid Request")
		}
	}
	if response == nil {
		response = make(map[string]interface{})
	}
	response["username"] = username
	// Call prepare response to frontend
	c.PrepareResponse(response, err)

}

// PrepareResponse is function to prepare the response data
func (c *ServiceController) PrepareResponse(data map[string]interface{}, err error) {
	// Make Response
	if err == nil {
		c.Context.Info("Result %v", data)
		c.Context.ResponseJson(data)
	} else {
		var message = err.Error()
		var status = 500
		// 500 Internal Server Error
		// 401 Unauthorized
		// 411 Invalid Request
		// 504 Gateway Timeout
		if strings.Contains(message, "Length Required") {
			status = 411
		} else if strings.Contains(message, "Invalid Credentials") {
			// LDAP Result Code
			status = 401
		} else if strings.Contains(message, "User does not exist") {
			// LDAP Result Code
			status = 401
		} else if strings.Contains(message, "Too many entries returned") {
			// LDAP Result Code
			status = 401
		} else if strings.Contains(message, "Network Error") {
			// LDAP Result Code
			status = 504
			message = "Communication Error or Timeout"
		} else if strings.HasPrefix(strings.ToLower(message), "notpass") {
			// WS Result Code
			status = 401
		} else if strings.Contains(message, "i/o timeout") {
			// WS Result Code
			status = 504
			message = "Communication Error or Timeout"
		} else if strings.Contains(message, "connection timed out") {
			// WS Result Code
			status = 504
			message = "Communication Error or Timeout"
		} else if strings.Contains(message, "Timeout connect") {
			// WS Result Code
			status = 504
			message = "Communication Error or Timeout"
		}

		data["message"] = message

		c.Context.Info("Error %v %v", status, err)
		c.Context.Response(status, message)
	}

}
