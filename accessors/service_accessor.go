package accessors

import (
	"sandbox/conicle-mtl-authen/common"
	"techberry-go/common/facade"
)

// ServiceAccessor is the main controller
type ServiceAccessor struct {
	Context       facade.AppContext
	Properties    facade.Properties
	Connector     facade.Connector
	Handler       facade.Handler
	ServiceCommon common.ServiceCommon
	Version       string
}
