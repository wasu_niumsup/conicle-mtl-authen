package accessors

import (
	"crypto/tls"
	"errors"
	"net/http"
	"time"

	"github.com/tiaguinho/gosoap"
)

// CheckAgentRequest Data
type CheckAgentRequest struct {
	Username string
	Password string
}

// CheckAgentResponse Data
type CheckAgentResponse struct {
	Result     string `xml:"fld_result"`
	SesstionID string `xml:"fld_sessionID"`
	Username   string `xml:"fld_agent_number"`
	Name       string `xml:"fld_agent_name"`
	Department string `xml:"fld_agent_department"`
	Position   string `xml:"fld_agent_position"`
	Type       string `xml:"fld_agent_type"`
}

// SoapBuildRequest build data request
func (r CheckAgentRequest) SoapBuildRequest() *gosoap.Request {
	return gosoap.NewRequest("CheckAgentAuthentication", gosoap.Params{
		"fld_agent_number":   r.Username,
		"fld_agent_password": r.Password,
	})
}

// SOAPAuthenticate shows how a typical application can verify a login attempt
func (c *ServiceAccessor) SOAPAuthenticate(username string, password string) (map[string]interface{}, error) {
	var response map[string]interface{}

	var soapRes *gosoap.Response
	var wsdl = c.Properties.String("wsdl::location")
	soap, err := gosoap.SoapClientWithConfig(wsdl,
		&http.Client{
			Timeout: 4 * time.Minute,
			Transport: &http.Transport{
				TLSClientConfig: &tls.Config{
					InsecureSkipVerify: true,
				},
				MaxIdleConns:    100,
				IdleConnTimeout: 30 * time.Second,
			}},
		&gosoap.Config{Dump: true},
	)
	if err != nil {
		return nil, err
	}
	soapRes, err = soap.CallByStruct(CheckAgentRequest{
		Username: username,
		Password: password,
	})
	if err != nil {
		return nil, err
	}

	var chkAgent CheckAgentResponse
	soapRes.Unmarshal(&chkAgent)

	if chkAgent.Result != "passed" {
		return nil, errors.New(chkAgent.Result)
	}
	response = make(map[string]interface{})
	response["username"] = username
	response["employee_id"] = ""
	response["agent_name"] = chkAgent.Name
	return response, err
}
