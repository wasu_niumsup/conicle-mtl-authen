package accessors

import (
	"techberry-go/common/facade"
)

// LDAPAuthenticate shows how a typical application can verify a login attempt
func (c *ServiceAccessor) LDAPAuthenticate(username string, password string) (map[string]interface{}, error) {
	var response map[string]interface{}
	var ldapConfig *facade.LDAPConfig
	ldapConfig = &facade.LDAPConfig{
		Base:               c.Properties.DefaultString("ldap::base", "OU=Bussiness Units,DC=muangthai,DC=co,DC=th"),
		Host:               c.Properties.DefaultString("ldap::server.host", ""),
		ServerName:         c.Properties.DefaultString("ldap::server.name", ""),
		Port:               c.Properties.DefaultInt("ldap::server.port", 636),
		UseSSL:             c.Properties.DefaultBool("ldap::server.usessl", true),
		SkipTLS:            c.Properties.DefaultBool("ldap::server.skiptls", true),
		BindDN:             c.Properties.DefaultString("ldap::service.account.user", username),
		BindPassword:       c.Properties.DefaultString("ldap::service.account.password", password),
		UserFilter:         c.Properties.DefaultString("ldap::userfilter", "(sAMAccountName=%s)"),
		Attributes:         c.Properties.DefaultStrings("ldap::attributes", []string{"employeeID"}),
		TrustCertFilePath:  c.Properties.DefaultString("ldap::trustcertfilepath", ""),
		InsecureSkipVerify: true,
	}
	client := c.Connector.LDAP(ldapConfig)

	/*
		client := &ldap.LDAPClient{
			Base:         c.Properties.DefaultString("ldap::base", "OU=Bussiness Units,DC=muangthai,DC=co,DC=th"),
			Host:         c.Properties.DefaultString("ldap::server.address", ""),
			ServerName:   c.Properties.DefaultString("ldap::domain", ""),
			Port:         c.Properties.DefaultInt("ldap::server.port", 636),
			UseSSL:       c.Properties.DefaultBool("ldap::server.usessl", true),
			SkipTLS:      c.Properties.DefaultBool("ldap::server.skiptls", true),
			BindDN:       c.Properties.DefaultString("ldap::service.account.user", username),
			BindPassword: c.Properties.DefaultString("ldap::service.account.password", password),
			UserFilter:   c.Properties.DefaultString("ldap::userfilter", "(sAMAccountName=%s)"),
			Attributes:   c.Properties.DefaultStrings("ldap::attributes", []string{"employeeID"}),
		}
	*/
	defer client.Close()
	ok, res, err := client.Authenticate(username, password)
	if ok {
		response = make(map[string]interface{})
		response["username"] = username
		response["employee_id"] = res["employeeID"]
		response["agent_name"] = ""
		if err != nil {
			c.Context.SysOutput("Error ", username, err)
			err = nil
		}
	}
	return response, err
}
